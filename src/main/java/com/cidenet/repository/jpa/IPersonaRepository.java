package com.cidenet.repository.jpa;

import com.cidenet.model.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(
        isolation = Isolation.READ_COMMITTED
)
public interface IPersonaRepository extends JpaRepository<Persona, Long> {
    @Query("select perso from Persona perso where perso.numeroIdentificacion = :id")
    Persona consultarPersonaId(@Param("id") final String id);

    @Query("select perso from Persona perso WHERE perso.numeroIdentificacion=:numeroIdentificacion")
    Persona consultarIdentificacion(@Param("numeroIdentificacion") String numeroIdentificacion);
}
