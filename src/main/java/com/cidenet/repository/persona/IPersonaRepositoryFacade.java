package com.cidenet.repository.persona;

import com.cidenet.commons.util.exception.WebClientException;
import com.cidenet.model.entities.Persona;

import java.util.Collection;
import java.util.Optional;

public interface IPersonaRepositoryFacade {


    Optional<Persona> createUser(final Persona persona) throws WebClientException;

    Optional<Collection<Persona>> getUser() throws WebClientException;

    Optional<Integer> deleteUser(String numIdentificacion) throws WebClientException;


}
