package com.cidenet.repository.persona.impl;

import com.cidenet.commons.util.exception.WebClientException;
import com.cidenet.commons.util.log.LogUtils;
import com.cidenet.model.entities.Persona;
import com.cidenet.repository.jpa.IPersonaRepository;
import com.cidenet.repository.persona.IPersonaRepositoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class PersonaRepositoryImpl implements IPersonaRepositoryFacade {

    @Autowired
    private IPersonaRepository personaRepository;
    @PersistenceContext
    EntityManager em;
    @Value("${update.data}")
    String actualizarData;
    @Value("${update.data.fail}")
    String actualizarError;
    @Value("${bussines.status.code}")
    String codBussines;
    @Value("${update.data}")
    String exito;
    @Value("${update.data.fail}")
    String error;

    public PersonaRepositoryImpl() {
    }

    public Optional<Persona> createUser(Persona persona) throws WebClientException {
        LogUtils.info("PersonaRepositoryImpl.Creando usuario ");
        Optional<Persona> resPersona = Optional.ofNullable(personaRepository.save(persona));
        return Optional.of(resPersona.get());
    }

    public Optional<Collection<Persona>> getUser() {
        LogUtils.info("PersonaRepositoryImpl. consultando Usuarios.");
        Collection<Persona> user = new ArrayList(this.personaRepository.findAll());
        return Optional.of(user);
    }

    @Transactional(
            propagation = Propagation.REQUIRED
    )
    public Optional<Integer> deleteUser(String numIdentificacion) throws WebClientException {
        LogUtils.info("PersonaRepositoryImpl. Eliminando Usuario con id ." + numIdentificacion);

        try {
            Query query = this.em.createQuery("DELETE FROM Persona e WHERE e.numeroIdentificacion= :numIdentificacion");
            query.setParameter("numIdentificacion", numIdentificacion);
            int rowsDeleted = query.executeUpdate();
            em.close();
            return Optional.of(rowsDeleted);
        } catch (WebClientException var4) {
            LogUtils.error(var4);
            throw new WebClientException(var4.getCode(), var4.getMessage());
        } catch (Exception var5) {
            throw new WebClientException(this.codBussines, var5.getMessage());
        }
    }


}
