
package com.cidenet.web.api.rest.personas.impl;

import com.cidenet.commons.constants.api.persona.EndpointPersonaApi;
import com.cidenet.commons.domains.request.persona.ActualizarUsuarioRequest;
import com.cidenet.commons.domains.request.persona.CrearUsuarioRequest;
import com.cidenet.commons.domains.response.builder.ResponseBuilder;
import com.cidenet.commons.domains.response.personas.CrearUsuarioResponse;
import com.cidenet.commons.enums.TransactionState;
import com.cidenet.commons.util.exception.ValidationUtils;
import com.cidenet.commons.util.exception.WebClientException;
import com.cidenet.commons.util.log.LogUtils;
import com.cidenet.service.persona.IPersonaService;
import com.cidenet.web.api.rest.personas.IPersonaApiController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping({"cidenet/persona/"})
@CrossOrigin(
        origins = {"*"},
        methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT}
)
public class PersonaApiController implements IPersonaApiController {
    private final IPersonaService personaService;
    @Value("${status.ok}")
    String OK;
    @Value("${bussines.status.code}")
    String codBussines;
    @Value("${bussines.status.fail}")
    String codBussinesFail;
    @Value("${bussines.status.value}")
    String listaVacia;
    @Value("${create.error}")
    String errorInsercion;
    @Value("${datos.consulta.exito}")
    String exitoConsulta;
    @Value("${update.data}")
    String actualizarData;

    public PersonaApiController(IPersonaService personaService) {
        this.personaService = personaService;
    }

    @Override
    @PostMapping(EndpointPersonaApi.REGISTRAR)
    public ResponseEntity createUser(@RequestBody CrearUsuarioRequest user, HttpServletRequest request) {
        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            ValidationUtils.validateNullEmptyObject(user);
            Optional<CrearUsuarioResponse> createdPerson = personaService.crearUsuario(user);
            if (createdPerson.isPresent()) {
                response.withStatus(createdPerson.get() != null ? HttpStatus.CREATED : HttpStatus.OK)
                        .withBusinessStatus(OK)
                        .withMessage("Usuario Creado")
                        .withResponse(createdPerson)
                        .withTransactionState(TransactionState.OK)
                        .buildResponse();
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.OK)
                    .withBusinessStatus(e.getCode())
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.OK)
                    .withBusinessStatus(codBussinesFail)
                    .withMessage(e.getMessage());
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @Override
    @GetMapping(EndpointPersonaApi.GET_USER)
    public ResponseEntity getUser(HttpServletRequest servletRequest) {
        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            Optional<Collection<CrearUsuarioResponse>> user = personaService.getUser();
            response.withStatus(user.isPresent() ? HttpStatus.OK : HttpStatus.CONFLICT)
                    .withBusinessStatus(OK)
                    .withResponse(user.isPresent() ? user : new CrearUsuarioRequest())
                    .withTransactionState(TransactionState.OK)
                    .buildResponse();

        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(this.codBussines)
                    .withTransactionState(TransactionState.FAIL)
                    .withMessage(e.getMessage());
        } finally {
            return response.withPath(servletRequest
                    .getRequestURI()).
                    buildResponse();
        }
    }

    @Override
    @PutMapping(EndpointPersonaApi.UPD_USER)
    public ResponseEntity updateUser(@RequestBody ActualizarUsuarioRequest upd, HttpServletRequest request) {
        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            ValidationUtils.validateNullEmptyObject(upd);
            Optional<String> updResponse = personaService.updateUser(upd);
            if (updResponse.isPresent() && (updResponse.get()).equals(actualizarData)) {
                response.withStatus(HttpStatus.OK)
                        .withBusinessStatus(OK)
                        .withMessage((String) updResponse.get())
                        .withTransactionState(TransactionState.OK)
                        .buildResponse();
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withTransactionState(TransactionState.FAIL)
                    .withMessage(e.getMessage());
        } catch (Exception var10) {
            LogUtils.error(var10);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(codBussines)
                    .withTransactionState(TransactionState.FAIL)
                    .withMessage(var10.getMessage());
        } finally {
            return response.withPath(request
                    .getRequestURI())
                    .buildResponse();
        }
    }

    @DeleteMapping(EndpointPersonaApi.DEL_USER)
    public ResponseEntity deleteUser(@PathVariable String numIdentificacion, HttpServletRequest request) {
        ResponseBuilder response = ResponseBuilder.newBuilder();

        try {
            Optional<String> delete = personaService.deleteUser(numIdentificacion);
            if (delete.isPresent()) {
                response.withStatus(delete.get() != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
                        .withBusinessStatus(OK)
                        .withMessage(delete.get())
                        .withTransactionState(TransactionState.OK)
                        .buildResponse();
            }
        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(e.getCode())
                    .withTransactionState(TransactionState.FAIL)
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withBusinessStatus(this.codBussines)
                    .withTransactionState(TransactionState.FAIL)
                    .withMessage(e.getMessage());
        } finally {
            return response.withPath(request.getRequestURI()).buildResponse();
        }
    }
}
