package com.cidenet.web.api.rest.personas;

import com.cidenet.commons.domains.request.persona.ActualizarUsuarioRequest;
import com.cidenet.commons.domains.request.persona.CrearUsuarioRequest;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IPersonaApiController {
    ResponseEntity createUser(final CrearUsuarioRequest user, HttpServletRequest request);

    ResponseEntity getUser(HttpServletRequest servletRequest);

    ResponseEntity updateUser(final ActualizarUsuarioRequest upd, HttpServletRequest request);

    ResponseEntity deleteUser(final String numIdentificacion, HttpServletRequest request);
}
