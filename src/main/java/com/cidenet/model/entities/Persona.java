package com.cidenet.model.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
@Table(name = "PERSONA")
public class Persona implements Serializable {
    @Id
    @GeneratedValue(
            generator = "PERSONA_GENERATOR"
    )
    @Column(
            name = "id_persona"
    )
    @SequenceGenerator(
            name = "PERSONA_GENERATOR",
            sequenceName = "PERSONA_SEQ",
            allocationSize = 1
    )
    private Long id;
    @Column(
            name = "PRIMER_NOMBRE",
            length = 100
    )
    private String primerNombre;
    @Column(
            name = "SEGUNDO_NOMBRE",
            length = 100
    )
    private String segundoNombre;
    @Column(
            name = "PRIMER_APELLIDO",
            length = 100
    )
    private String primerApellido;
    @Column(
            name = "SEGUNDO_APELLIDO",
            length = 100
    )
    private String segundoApellido;
    @Column(
            name = "TIPO_IDENTIFICACION"
    )
    private String tipoIdentificacion;
    @Column(
            name = "NUMERO_IDENTIFICACION",
            nullable = false,
            unique = true
    )
    private String numeroIdentificacion;
    @Column(
            name = "PAIS"
    )
    private String pais;
    @Column(
            name = "CORREO_ELECTRONICO"
    )
    private String correoElectronico;
    @Temporal(TemporalType.DATE)
    @Column(
            name = "FECHA_INGRESO"
    )
    private Date fechaIngreso;
    @Column(
            name = "AREA"
    )
    private String area;
    @Column(
            name = "ESTADO"
    )
    private String estado;
    @Column(
            name = "FECHA_REGISTRO"
    )
    private String fechaRegistro;
    @Temporal(TemporalType.DATE)
    @Column(
            name = "FECHA_MODIFICACION"
    )
    private Date fechaModificacion;


}
