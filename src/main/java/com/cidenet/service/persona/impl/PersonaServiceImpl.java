package com.cidenet.service.persona.impl;

import com.cidenet.commons.converter.PersonaConverter;
import com.cidenet.commons.domains.request.persona.ActualizarUsuarioRequest;
import com.cidenet.commons.domains.request.persona.CrearUsuarioRequest;
import com.cidenet.commons.domains.response.personas.CrearUsuarioResponse;
import com.cidenet.commons.util.exception.WebClientException;
import com.cidenet.commons.util.log.LogUtils;
import com.cidenet.model.entities.Persona;
import com.cidenet.repository.jpa.IPersonaRepository;
import com.cidenet.repository.persona.IPersonaRepositoryFacade;
import com.cidenet.service.persona.IPersonaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonaServiceImpl implements IPersonaService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private IPersonaRepositoryFacade personaRepositoryFacade;
    @Autowired
    private IPersonaRepository personaRepository;
    @Value("${update.data}")
    String actualizarData;
    @Value("${update.data.fail}")
    String actualizarError;
    @Value("${bussines.status.code}")
    String codBussines;
    @Value("${persona.duplicada}")
    String Duplicada;
    @Value("${cidenet.colombia}")
    String CorreoColombia;
    @Value("${cidenet.usa}")
    String CorreoUSA;
    @Value("${colombia}")
    String Colombia;
    @Value("${usa}")
    String USA;
    @Value("${usuario.eliminado}")
    String Eliminado;

    public PersonaServiceImpl() {
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Optional<CrearUsuarioResponse> crearUsuario(CrearUsuarioRequest user) throws WebClientException, ParseException {
        Optional<Persona> personaEntity = Optional.of(new Persona());

        if (personaRepository.consultarIdentificacion(user.getNumeroIdentificacion()) != null) {
            throw new WebClientException(Duplicada);
        } else {
            PersonaConverter.crearUsuarioDtoToEntity(this.modelMapper, user, personaEntity);
            Optional<Persona> personaCreada = this.personaRepositoryFacade.createUser(personaEntity.get());
            return Optional.ofNullable(modelMapper.map(personaCreada.get(), CrearUsuarioResponse.class));
        }
    }

    public Optional<Collection<CrearUsuarioResponse>> getUser() throws WebClientException {
        return Optional.ofNullable(personaRepositoryFacade.getUser().get().stream()
                .map(data -> modelMapper.map(data, CrearUsuarioResponse.class))
                .collect(Collectors.toList()));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Optional<String> updateUser(final ActualizarUsuarioRequest upd) throws WebClientException {
        String correoNuevo = "";
        Optional<Persona> persona = Optional.of(new Persona());

        try {
            Optional<Persona> findPersona = Optional.of(personaRepository.consultarPersonaId(upd.getAntiguoNumeroIdentificacion()));
            if (!findPersona.isPresent()) {
                LogUtils.info("No se encontro la persona por el id : " + upd.getAntiguoNumeroIdentificacion());
                throw new WebClientException(codBussines, "No se encontro la persona a actualizar con el id: " + upd.getAntiguoNumeroIdentificacion());
            } else {
                if (!upd.getPrimerNombre().equals((findPersona.get()).getPrimerNombre()) ||
                        !upd.getPrimerApellido().equals((findPersona.get()).getPrimerApellido())) {
                    if (upd.getPais().equals(Colombia)) {
                        correoNuevo = upd.getPrimerNombre().concat(".").concat(upd.getPrimerApellido()).concat(CorreoColombia).toLowerCase();
                    } else if (upd.getPais().equals(USA)) {
                        correoNuevo = upd.getPrimerNombre().concat(".").concat(upd.getPrimerApellido()).concat(CorreoUSA).toLowerCase();
                    }
                }

                PersonaConverter.updUserConvert(modelMapper, upd, persona);
                findPersona.get().setPrimerNombre(persona.get().getPrimerNombre());
                findPersona.get().setPrimerApellido(persona.get().getPrimerApellido());
                findPersona.get().setSegundoNombre(persona.get().getSegundoNombre());
                findPersona.get().setSegundoApellido(persona.get().getSegundoApellido());
                findPersona.get().setTipoIdentificacion(persona.get().getTipoIdentificacion());
                findPersona.get().setNumeroIdentificacion(persona.get().getNumeroIdentificacion());
                findPersona.get().setPais(persona.get().getPais());
                if (correoNuevo.equals("")) {
                    findPersona.get().setCorreoElectronico(findPersona.get().getCorreoElectronico());
                } else {
                    findPersona.get().setCorreoElectronico(correoNuevo);
                }

                findPersona.get().setArea(persona.get().getArea());
                findPersona.get().setEstado(persona.get().getEstado());
                findPersona.get().setFechaModificacion(new Date());
                personaRepository.save(findPersona.get());
                return Optional.of(actualizarData);
            }
        } catch (WebClientException var5) {
            throw new WebClientException(codBussines, var5.getMessage());
        }
    }

    public Optional<String> deleteUser(String numIdentificacion) throws WebClientException {

        Optional<Persona> personaEntity = Optional.of(this.personaRepository.consultarIdentificacion(numIdentificacion));

        if (personaEntity == null) {
            throw new WebClientException("No existe una persona con ese Numero de Identificación");
        } else {
            personaRepositoryFacade.deleteUser(personaEntity.get().getNumeroIdentificacion());
            return Optional.of(Eliminado);
        }
    }
}
