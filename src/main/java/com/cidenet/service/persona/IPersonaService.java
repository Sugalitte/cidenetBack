
package com.cidenet.service.persona;

import com.cidenet.commons.domains.request.persona.ActualizarUsuarioRequest;
import com.cidenet.commons.domains.request.persona.CrearUsuarioRequest;
import com.cidenet.commons.domains.response.personas.CrearUsuarioResponse;
import com.cidenet.commons.util.exception.WebClientException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Optional;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface IPersonaService {
    Optional<CrearUsuarioResponse> crearUsuario(final CrearUsuarioRequest user) throws WebClientException, ParseException;

    Optional<Collection<CrearUsuarioResponse>> getUser() throws WebClientException;

    Optional<String> updateUser(final ActualizarUsuarioRequest upd) throws WebClientException;

    Optional<String> deleteUser(final String numIdentificacion) throws WebClientException;
}
