package com.cidenet.commons.domains.response.personas;

import com.cidenet.commons.domains.request.persona.CrearUsuarioRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class CrearUsuarioResponse {

    private static final long serialVersionUID = 1L;
    private CrearUsuarioRequest persona;
}
