package com.cidenet.commons.domains.request.persona;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class CrearUsuarioRequest {

    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String pais;
    private String correoElectronico;
    private Date fechaIngreso;
    private String area;
    private String estado;
    private String fechaRegistro;

}
