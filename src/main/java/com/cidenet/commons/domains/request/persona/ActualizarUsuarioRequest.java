package com.cidenet.commons.domains.request.persona;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class ActualizarUsuarioRequest {

    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String antiguoNumeroIdentificacion;
    private String pais;
    private String correoElectronico;
    private String area;
    private String estado;
}
