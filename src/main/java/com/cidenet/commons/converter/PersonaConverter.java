package com.cidenet.commons.converter;

import com.cidenet.commons.domains.request.persona.ActualizarUsuarioRequest;
import com.cidenet.commons.domains.request.persona.CrearUsuarioRequest;
import com.cidenet.model.entities.Persona;
import org.modelmapper.ModelMapper;

import java.util.Optional;

public class PersonaConverter {


    public static void crearUsuarioDtoToEntity(ModelMapper modelMapper, CrearUsuarioRequest user, Optional<Persona> personaEntity) {
        modelMapper.map(user, personaEntity.get());
    }

    public static void updUserConvert(ModelMapper modelMapper, ActualizarUsuarioRequest upd, Optional<Persona> personaEntity) {
        modelMapper.map(upd, personaEntity.get());
    }
}
