package com.cidenet.commons.util.exception;

import com.cidenet.commons.util.annotations.RequiredParameter;
import com.cidenet.commons.util.annotations.RequiredPrimitiveParameter;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Component
public class ValidationUtils {


    public static void validateNullEmptyObject(Object obj) throws WebClientException {
        Field[] attr =  obj.getClass().getDeclaredFields();
        discoverNullEmptyObject(attr,obj);
        validateNullEmptyObjectSuperClass(obj);
    }


    public static void validateNullEmptyString(String value) throws WebClientException {
        if(value == null || "".equals(value.toString())) {
            throw new WebClientException("1000", "Error : Parametro de entrada vacío o nulo.");
        }
    }

    private static void validateNullEmptyObjectSuperClassImpl(Field[] fields,Object obj) throws WebClientException {
        discoverNullEmptyObject(fields,obj);
    }

    private static void validateNullEmptyObjectSuperClass(Object obj) throws WebClientException {
        validateNullEmptyObjectSuperClassImpl(obj.getClass().getSuperclass().getDeclaredFields(),obj);
    }

    private static void discoverNullEmptyObject(Field[] attr, Object obj) throws WebClientException{
        for (int i = 0; i < attr.length; i++) {
            final Field currentAttr = attr[i];
            currentAttr.setAccessible(true);

            if(currentAttr.isAnnotationPresent(RequiredParameter.class)
                    || currentAttr.isAnnotationPresent(RequiredPrimitiveParameter.class)){
                try {
                    Object value = currentAttr.get(obj);
                    System.out.println(currentAttr.getName()+" - "+value);
                    if(value == null || "".equals(value.toString())) {
                        throw new WebClientException("1000", "Error : Parametro Requerido [" + currentAttr.getName() + "] " + "vacío o nulo.");
                    }
                    if(!currentAttr.isAnnotationPresent(RequiredPrimitiveParameter.class)){
                        discoverNullEmptyObject(value.getClass().getDeclaredFields(),runGetter(currentAttr,obj));
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static Object runGetter(Field field, Object o) throws WebClientException{
        // MZ: Find the correct method
        for (Method method : o.getClass().getDeclaredMethods()) {
            if ((method.getName().startsWith("get"))
                    && (method.getName().length() == (field.getName().length() + 3))) {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    // MZ: Method found, run it
                    try {
                        return method.invoke(o,null);
                    } catch (IllegalAccessException e) {
                        System.out.println("Could not determine method: " + method.getName());
                        throw new WebClientException("1000","Could not determine method: " + method.getName());
                    } catch (InvocationTargetException e) {
                        System.out.println("Could not determine method: " + method.getName());
                        throw new WebClientException("1000","Could not determine method: " + method.getName());
                    }

                }
            }
        }
        return o;
    }


    public static String getTraceStringTraceException(Exception e){
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

}
