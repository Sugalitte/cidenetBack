package com.cidenet.commons.util.log;

public enum TipoLog {
    DEBUG, ERROR, FATAL, INFO, WARNING
}
