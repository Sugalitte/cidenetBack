package com.cidenet.commons.util.log;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;


/**
 *   Para usar la clase tiene que tener en el configuration.xml
 *
 * 
 * @author: jmedina

 * @version: 31/07/2019

 *

 * */
@Component
public class LogUtils {

    public LogUtils(){}


    @Autowired
    private void setPathLog(@Value("${path.LogUtils}") String value,
                            @Value("${path.local.root}") String localPath,
                            @Value("${prop.get.path.url}") String serverPathProperty,
                            @Value("${prop.root.log}") String propRootLog) {
        String baseUrl = "";
        try {
            baseUrl = System.getProperty(serverPathProperty).toString();
            baseUrl += File.separator;
        } catch (Exception e) {
            baseUrl = localPath;
        }
        System.setProperty(propRootLog,baseUrl+value);
    }

    /**
     * M�todo que registra los valores dependiendo de la clase, tipo y mensaje
     */
    private static void registrar(TipoLog tipo, String mensaje) {
            configuracionLog( tipo, "["+Thread.currentThread().getStackTrace()[3].getClassName()+"|" + Thread.currentThread().getStackTrace()[3].getMethodName()+":"+ Thread.currentThread().getStackTrace()[3].getLineNumber() + "] " + mensaje);
    }

    /**
     * M�todo que configura los valores dependiendo de la clase, tipo y mensaje
     */
    private static void configuracionLog(TipoLog tipo, String mensaje) {
        Logger LOGGER = LogManager.getRootLogger();
        registrarLog(LOGGER, tipo, mensaje);
    }


    /**
     * M�todo que configura los valores dependiendo de la clase, tipo y mensaje
     */
    private static void registrarLog(Logger LOGGER,TipoLog tipo, String mensaje) {

        switch (tipo) {
        case DEBUG:
            LOGGER.debug(mensaje);
            break;
        case ERROR:
            LOGGER.error(mensaje);
            break;
        case FATAL:
            LOGGER.fatal(mensaje);
            break;
        case INFO:
            LOGGER.info(mensaje);
            break;
        case WARNING:
            LOGGER.warn(mensaje);
        }

    }
    
    //Metodos Adaptadores
    /**
     * Log tipo INFO
     * @param mensaje
     */
    public static void info(String mensaje) {
        registrar(TipoLog.INFO,mensaje);
    }

    /**
     * Log tipo WARNING
     * @param mensaje
     */
    public static void warning(String mensaje) {
        registrar(TipoLog.WARNING,mensaje);
    }
    
    /**
     * Log tipo ERROR
     * @param mensaje
     */
    public static void error(String mensaje) {
        registrar(TipoLog.ERROR,mensaje);
    }
    
    /**
     * Log tipo ERROR
     * @param exception
     */
    public static void error(Exception exception) {
        StringWriter errors = new StringWriter();
        exception.printStackTrace(new PrintWriter(errors));
        registrar(TipoLog.ERROR,errors.toString());
    }
    
    /**
     * Log tipo DEBUG
     * @param mensaje
     */
    public static void debug(String mensaje) {
        registrar(TipoLog.DEBUG,mensaje);
    }
    
    /**
     * Log tipo FATAL
     * @param mensaje
     */
    public static void fatal(String mensaje) {
        registrar(TipoLog.FATAL,mensaje);
    }

    //Debug Methods ownertrace

    /**
     * Log tipo INFO
     * @param mensaje
     */
    public static void printInfoOwnerTrace(String mensaje) {
        configuracionLog(TipoLog.INFO,mensaje);
    }

    /**
     * Log tipo WARNING
     * @param mensaje
     */
    public static void printWarningOwnerTrace(String mensaje) {
        configuracionLog(TipoLog.WARNING,mensaje);
    }

    /**
     * Log tipo ERROR
     * @param mensaje
     */
    public static void printErrorOwnerTrace(String mensaje) {
        configuracionLog(TipoLog.ERROR,mensaje);
    }

    /**
     * Log tipo ERROR
     * @param exception
     */
    public static void printErrorOwnerTrace(Exception exception) {
        StringWriter errors = new StringWriter();
        exception.printStackTrace(new PrintWriter(errors));
        configuracionLog(TipoLog.ERROR,errors.toString());
    }

    /**
     * Log tipo DEBUG
     * @param mensaje
     */
    public static void printDebugOwnerTrace(String mensaje) {
        configuracionLog(TipoLog.DEBUG,mensaje);
    }

    /**
     * Log tipo FATAL
     * @param mensaje
     */
    public static void printFatalOwnerTrace(String mensaje) {
        configuracionLog(TipoLog.FATAL,mensaje);
    }

}
