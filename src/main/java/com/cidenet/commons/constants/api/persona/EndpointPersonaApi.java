package com.cidenet.commons.constants.api.persona;

public interface EndpointPersonaApi {
    String BASE_PERSONA = "cidenet/persona/";
    String REGISTRAR = "registro";
    String GET_USER = "getUser";
    String UPD_USER = "updUser";
    String DEL_USER = "deleteUser/{numIdentificacion}";
}
