package com.cidenet.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties
public class PersistenceConfigApp {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.primary")
    public JndiPropertyHolder primary() {
        return new JndiPropertyHolder();
    }

    @Bean(destroyMethod = "close")
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(primary().getDriverClassName());
        hikariConfig.setJdbcUrl(primary().getUrl());
        hikariConfig.setUsername(primary().getUsername());
        hikariConfig.setPassword(primary().getPassword());
        hikariConfig.setSchema(primary().getSchema());

        hikariConfig.setMaximumPoolSize(Integer.parseInt(primary().getMaxActive()));
        hikariConfig.setMinimumIdle(Integer.parseInt(primary().getMinIdle()));
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setPoolName("Cidenet-HikariPool");

        hikariConfig.addDataSourceProperty("dataSource.cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSqlLimit", "2048");
        hikariConfig.addDataSourceProperty("dataSource.useServerPrepStmts", "true");

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        return dataSource;
    }

    @Data
    @Getter
    @Setter
    @ToString
    private static class JndiPropertyHolder {
        private String jndiName;
        private String url;
        private String username;
        private String password;
        private String driverClassName;
        private String maxActive;
        private String maxIdle;
        private String minIdle;
        private String initialSize;
        private String removeAbandoned;
        private String schema;

    }

}
